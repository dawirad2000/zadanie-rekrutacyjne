<?php
/*
 * Template Name: Strona Główna
 */

get_header();

?>
	<main class="homepage">
			<section id="first_sec">
			<div class="page-container">
				<div class="header">
					<h1><?php the_field('first_sec_title'); ?></h1>
					<p><?php the_field('first_sec_desc'); ?></p>
				</div>
				<div class="gear">
					<img class="gear-img"  src="/wp-content/uploads/2023/01/gear.png">
					<div class="gear-txt-wrap">
						<p class="gear-txt"><?php the_field('gear_desc'); ?></p>
					</div>
					<div class="elements-wrap">
						<div class="elements-center">

					<?php

						$itemsCount = get_field('items_count');


						for($i = 0; $i < $itemsCount; $i++) {
							?>
						<div class="element">
							<div class="circle">
								<?php 
								$image = get_field('gear_icon');
								if( !empty( $image ) ): ?>
    								<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
								<?php endif; ?>
							</div>
							<div class="element-desc"><p><?php the_field('gear_icon_desc'); ?></p></div>
						</div>
							<?php
						}
					?>
						</div>
					</div>
				</div>
			</div>
			</section>

			<section id="second_sec">
				<div class="background">
				<?php 
				$image = get_field('second_sec_bg');
				if( !empty( $image ) ): ?>
    				<img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
				<?php endif; ?>
				</div>
				<div class="form-wrap">
					<div class="page-container">
						<div class="form">
							<div class="text-box">
								<h1><?php the_field('second_sec_title'); ?></h1>
								<p><?php the_field('second_sec_desc'); ?></p>
							</div>
							<?php echo do_shortcode( '[contact-form-7 id="34" title="Formularz"]' ); ?>
						</div>
					</div>
				</div>
			</section>
	</main>
<?php
get_footer();

//Gear Script
jQuery(document).ready(function() {
  const screenWidth = jQuery(window).width();
  
  if (screenWidth > 1380) {
    jQuery(document).ready(function() {
      const container = jQuery('.elements-center');
      const items = jQuery('.element');
    
      const radius = 300;
      const total = items.length;
    
      const angle = (2 * Math.PI) / total;
    
      items.each(function(i) {
        const x = radius * Math.cos(angle * i);
        const y = radius * Math.sin(angle * i);
    
        jQuery(this).css({
          top: `${y}px`,
          left: `${x}px`,
        });
      
        
        const element = jQuery(this).find('.element-desc');
        const width = element.width() / 2;
    
        if (y === -300) {
          element.css('top', '-30px');
          element.css('left', `-${width - 20}px`);
          
        } else if (y === 300) {
          element.css('bottom', '-30px');
          element.css('left', `-${width - 20}px`);
        } else if (y !== 300 && x > 0) {
          element.css('left', '80px');
        } else if (y !== 300 && x < 0) {
          element.css('left', `-${element.width() + 40}px`);
        }
      });
    
    
    });
  } else {
    jQuery('.element').removeAttr('style');
    jQuery('.element-desc').removeAttr('style');
  }
});

jQuery(window).resize(function() {
    const screenWidth = jQuery(window).width();
    
    if (screenWidth > 1380) {
      jQuery(document).ready(function() {
        const container = jQuery('.elements-center');
        const items = jQuery('.element');
      
        const radius = 300;
        const total = items.length;
      
        const angle = (2 * Math.PI) / total;
      
        items.each(function(i) {
          const x = radius * Math.cos(angle * i);
          const y = radius * Math.sin(angle * i);
      
          jQuery(this).css({
            top: `${y}px`,
            left: `${x}px`,
          });
        
          
          const element = jQuery(this).find('.element-desc');
          const width = element.width() / 2;
      
          if (y === -300) {
            element.css('top', '-30px');
            element.css('left', `-${width - 20}px`);
            
          } else if (y === 300) {
            element.css('bottom', '-30px');
            element.css('left', `-${width - 20}px`);
          } else if (y !== 300 && x > 0) {
            element.css('left', '80px');
          } else if (y !== 300 && x < 0) {
            element.css('left', `-${element.width() + 40}px`);
          }
        });
      
      
      });
    } else {
      jQuery('.element').removeAttr('style');
      jQuery('.element-desc').removeAttr('style');
    }
  });